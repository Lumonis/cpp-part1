//size_t 
//  7 = 111
// 15 = 1111
// 10 = 01010
//  6 = 0110
//  7 = 0111
// 15 = 1111
// 15 = 1111
//  7 = 0111
//  0 = 00000
//  3 = 11
// Rest are 0
//cout << hex << ... // Display the variable as a hexadecimal value


#include <iostream>
using namespace std;

struct bitfield_struct {        // Defining a union bitfield and the size of its entries
    unsigned int number0:  1;  // Will hold a 0
    unsigned int number1:  3;  // Will hold a 7
    unsigned int number2:  4;  // Will hold a 15
    unsigned int number3:  5;  // Will hold a 10
    unsigned int number4:  4;  // Will hold a 6
    unsigned int number5:  4;  // Will hold a 7
    unsigned int number6:  4;  // Will hold a 15
    unsigned int number7:  4;  // Will hold a 15
    unsigned int number8:  4;  // Will hold a 7
    unsigned int number9:  5;  // Will hold a 0
    unsigned int number10: 2;  // Will hold a 3
    unsigned int zeroes:   5;  // Will hold's 0
};

union bitfield_union {          // Defining an "object" that can contain a field or size_t???
    bitfield_struct field;      // The union will be able to store object of the form specified by bitfield_struct
    size_t output_size_t;       // The union will be able to store object of the size_t form????
    int output_int;
};

int main() 
{
    bitfield_union exercizeArray;                                                   // Create a object?
    exercizeArray.field = {0, 7, 15, 10, 6, 7, 15, 15, 7, 0, 3};                    // Imputing a struct of data
    cout << hex <<exercizeArray.output_size_t << "\n";                              // Outputting the combined data as a hex string
    cout << hex <<exercizeArray.output_int << "\n";
}
