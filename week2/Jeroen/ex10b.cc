//file      : ex10.cc
//function  : Determining sets of characters
//version   : 1.0
//author    : Jeroen Lammers
//c++ ex    : ex 10

#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
    for (size_t index = 0; index != 127; ++index)       // Looping over all Ascii options
    {
	cout << index << " = " << (char) index << "\n";
    }
    
}
