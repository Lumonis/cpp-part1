//file      : ex13.cc
//function  : Writing a filter to remove empty lines at the start and end of file
//version   : 1.0
//author    : Jeroen Lammers
//c++ ex    : ex 13

#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
    string line; 		// A string to store read lines in

    // Removing all empty lines from the start
    while ( true )
    {
        getline(cin, line);
        if ( line != "")
            break;
    }

    // Printing first non empty line
    cout << line << "\n";

    // Counter to keep track of lines removed before end
    size_t emptyLineCounter = 0;


    // Proccessing the line after the first nonempty line
    while ( true )			
    {
        if ( not getline(cin, line) )	// Reading lines
             break;

        // Processing non empty lines
        if ( line.size() != 0 )
        {
            // Delaid adding of empty lines if not end of file
            for (; 0 < emptyLineCounter; --emptyLineCounter )
                cout << "\n";

            cout << line << "\n";	// Outputting line
        }

        // Keeping track of the skipped empty lines
        else
            ++emptyLineCounter;
    }
}
