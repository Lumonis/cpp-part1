//file      : ex12.cc
//function  : Replacing the first word by the last
//version   : 1.0
//author    : Jeroen Lammers
//c++ ex    : ex 12

#include <iostream>
#include <string>

using namespace std;

int main()
{
    while (true)
    {
        string line;
        if (not getline(cin, line))
             break;

        // Finding the start of the last word
        size_t posLastSpace = line.find_last_of(" ");

        // Finding the end of the first word
        size_t posFirstSpace = line.find_first_of(" ");


        // Checking for extra spaces at start and end
        if ( posLastSpace == line.length() )
            cout << "ERROR please remove acces spaces at the end\n";
        if ( posFirstSpace == 0 )
            cout << "Error please remove acces spaces at the start\n";

        // If there are not spaces (0 or 1 word) then finish
        if (posLastSpace == string::npos)
            cout << line << "\n";

        // Replace the first word by the last (2 or more words)
        else
        {
            // Calculating index and length of last word
            size_t lastWordStart = posLastSpace + 1;
            size_t lastWordLength = line.length() - posLastSpace;

            // Defining a substring containing the last word
            string lastWord = line.substr(lastWordStart, lastWordLength);

            // Replacing the first word by the lastword string
            string output = line.replace(0, posFirstSpace, lastWord);

            cout << output << "\n";
        }
    }
}
