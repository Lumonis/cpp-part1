//file      : ex15a.cc
//function  : Rotational bitshifting
//version   : 1.0
//author    : Jeroen Lammers
//c++ ex    : ex 15

#include <iostream>
#include <string>
#include <bitset>

using namespace std;

int main(int argc, char *argv[])
{
    string numImput = argv[1];
    size_t binImput = stoi(numImput, nullptr, 2);
    string argument = argv[2];
    size_t sizeImput = sizeof(binImput);

    size_t output;

    if(argument == "rol")
    {
	output = (binImput << 1) + (binImput >> sizeImput);
    }
    else if(argument == "ror")
    {
	output = (binImput >> 1) + (binImput << sizeImput);
    }

    cout << "imput:  " << bitset<32>(binImput) << " : " << binImput << "\n";
    cout << "output: " << bitset<32>(output)   << " : " << output   << "\n";
}
