//file      : ex09.cc
//function  : printing a multiplication table
//version   : 1.0
//author    : Jeroen Lammers
//c++ ex    : ex9

#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
    int imput = stoi(  argv[1] );                     // Assigning a normal name
    for(size_t index = 1; index != 11; ++index)       // A for loop to construct the table
    {
        cout << index << " * " << imput << " = " << index*imput << "\n";
    }
}
