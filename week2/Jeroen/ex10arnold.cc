//Author: Arnold Dongelmans

#include <iostream>
#include <ctype.h>

using namespace std;

int main()
{
    string allasciiletters; // Declare string allasciicharacters

    // For loop until length of ascii characterset (=128 long (non-extended))
    for (int characternumber = 0; characternumber <= 127; ++characternumber)
	{
	// If the character is a letter
        if ( isalpha( char(characternumber) ))
	{
            allasciiletters += ( char(characternumber) );
	    // Then add this letter to string allasciicharacters
        }
    }
    cout << allasciiletters << "\n";
}
