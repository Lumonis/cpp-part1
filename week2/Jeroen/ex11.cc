//file      : ex11.cc
//function  : Reading a file
//version   : 1.0
//author    : Jeroen Lammers
//c++ ex    : ex 11

// Example command cat ex11.cc | ./ex11Exe

#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
    if(argc == 0)                                  // Checking for correct parameter number
    {
        cout << "please submt an argument";
	exit(0);
    }

    string parameter1 = argv[1];                   // Definging the parameters name for later use
    size_t linenumber = 0;                         // Defining a unsigned int to store the line number

    if(parameter1 == "ok")                         // If the correct parameter was given, use standard form
    {
        while (true)
	{
//	    cout << "? ";
	    string line;
            if (not getline(cin, line)) break;     // EOF test
            ++linenumber;			   // Increment linenumber counter
        }
    }
    else                                           // If the wrong parameter was given, use wrong method
    {
	while (true)
	{
//	    cout << "? ";
	    string line;
	    ++linenumber;			   // Increment linenumber counter
	    if (not getline(cin, line)) break;	   // EOF test
	}
    }
    cout << "Total linecount: " << linenumber << "\n";  // Print total line number
}
