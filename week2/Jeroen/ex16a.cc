//file      : ex16a.cc
//function  : Converting a base 10 number to base N (0 < N < 37)
//version   : 1.0
//author    : Jeroen Lammers
//c++ ex    : ex 16

#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
    if(argc < 2)          // Checking correct amount of arguments
    {
        cout << "Please provide enough arguments!\n";
        return EXIT_FAILURE;
    }

    // Defining names to arguments for ease of reading
    size_t radix  = stoul( argv[1] );
    size_t number = stoul( argv[2] );

    string output;

    // Defininf a list of characters to use for each number
    string const numberList = "0123456789abcdefghijklmnopqrstuvwxyz";

    while ( number != 0 )
    {
        size_t digit = number % radix;      // Finding lsd
        number = number / radix;            // Removing lsd from number

        // Converting digit to the correct character
        output.insert(0, numberList, digit, 1);
    }

    if ( output.length() == 0 )		// If output is empty then it should be 0
        output = "0";

    // Reverting string and defining output format
    cout << argv[2] << ", displayed using radix " << radix << " is: ";
    cout << output << "\n";
}
