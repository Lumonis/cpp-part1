// main.cc
// Jeroen Lammers

#include "main.ih"

int main()
{
    int square[10][10];

    int (*row)[10] = &square[0];
    inv_identity(row);

    // Displaying the array
    for ( size_t row = 0; row != 10; ++row )
    {
        for ( size_t col = 0; col != 10; ++col )
            cout << square[row][col] << ' ';
        cout << '\n';
    }
}
