#include <iostream>

using namespace std;

int main()
{
    int array[10] = {0,1,2,3,4,5,6,7,8,9};
    int *col = &array[0]; // pointer union
    cout << col << '\n';
    cout << *(col + 4) << '\n';
    cout << *++col << '\n';
/*
    int row[10] = {1,2,3,4,5,6,7,8,9,0};
    int *rowp = NULL;
    rowp = rowp + 1;

    cout << *row << '\n';
    cout << *(&row[0] + NULL) << '\n';
    cout << *(row + 1) << '\n';
*/

    int square[3][3] = {{1,2,3},{4,5,6},{7,8,9}};
    int (*row)[3] = &square[0];

    for (size_t idx = 0; idx != 3; ++idx)
        cout << **(row + idx) << '\n';
}
