// invidentity.cc

#include "main.ih"

void inv_identity(int (*row)[10])
{
    // Looping over rows in matrix
    for (int (*rowLoop)[10] = row; rowLoop != row + 10; ++rowLoop )
    {
        // Looping over elements inside a row
        for (int *col = rowLoop[0], *initCol = col; col != initCol + 10; ++col)
            // Checking relative position in row and col
            col - initCol == rowLoop - row ? *col = 0 : *col = 1;
    }
}
