// main.cc
// Jeroen Lammers
// ex34

#include <iostream>
#include <string>

using namespace std;

extern char **environ;

int main(int argc, char *argv[])
{
    int difference = argv - environ;

    // Finding the size of environ
    size_t envc = 0;
    while( environ[envc] )
        ++envc;

    cout << "------ENVI------\n"; // Added for clariity in output

    // Looping over all environ variables using argv
    for (size_t idx = 0; idx != envc; ++idx)
        cout << *(argv - difference + idx) << '\n';

    cout << "\n------ARGV------\n"; // Added for clarity in output

    // Looping over all argv variables using env
    size_t argcul = argc; // note: we know argc > 0 always
    for (size_t idx = 0; idx != argcul; ++idx)
        cout << *(environ + difference + idx) << '\n';
}
