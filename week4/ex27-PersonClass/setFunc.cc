// setFunc.cc
// Jeroen Lammers
// ex 27

#include "person.h"

using namespace std;

void Person::setName(string const &name)
{
    d_name = name;
}

void Person::setAddress(string const &address)
{
    d_address = address;
}

void Person::setPhone(string const &phone)
{
    if (phone.find_first_not_of("0123456789") == string::npos)
        d_phone = phone;
    else if (phone.empty())
        d_phone = " - not avalible - ";
    else
        cout << "A phone number may only contain digits\n";
}

void Person::setMass(size_t mass)
{
    d_mass = mass;
}
