#include <string>

class Person
{
    std::string d_name;     // name of person
    std::string d_address;  // adress field
    std::string d_phone;    // elephone number
    size_t      d_mass;     // the mass in kg.

    public:                 // member functions
        void setName(std::string const &name);
        void setAddress(std::string const &adress);
        void setPhone(std::string const &phone);
        void setMass(tize_t mass);

        std::string const &name()    const;
        std::string const &address() const;
        std::string const &phone()   const;
        size_t mass()                const;

        void instert()

};
