//file      : sum1.cc
//function  : Summing a list of doubles
//version   : 1.0
//author    : Jeroen Lammers
//c++ ex    : ex 17

#include <iostream>
#include <initializer_list>

using namespace std;

double sum(initializer_list<double> list)
{
    int output = 0;
    
    for (size_t idx = 0, idx <= list.size())
    {
        output += list[idx];
    }
}
