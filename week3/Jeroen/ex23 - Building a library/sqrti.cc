//file      : sqrt1.cc
//function  : sqrt function
//author    : Jeroen Lammers
//c++ ex    : ex 23

#include <iostream>

using namespace std;

int sqrt(int arg)
{
    // Check if >0 then use ull sqrt
    if ( arg >= 0 )
        arg = -1 * arg;

    return sqrt( stoull(arg) );
}
