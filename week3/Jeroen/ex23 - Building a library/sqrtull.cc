//file      : sqrtull.cc
//function  : sqrt function
//author    : Jeroen Lammers
//c++ ex    : ex 23

#include <iostream>

using namespace std;

unsigned long long sqrt(unsigned long long arg)
{
    unsigned long long root = 0;

    if ( arg > 100 )       // Rest digits of root
    root = sqrt( arg / 100 ); // Finding previous digit

    if ( arg < 100 )       // First digit of root
    {
    // Volgens de opdracht moet dit een functie met tabel worden.....
        // Finding first digit of root rounding up
        for (; root * root < arg; ++root);
        // Manualy rounding the digit down
        --root;
        return root;
    }

    unsigned long long remainder;

    // Finding the remainder of the previous digit
    remainder = ( arg / 100 ) - root * root;

    // Prefixing the remainder to the next segment
    arg = remainder * 100 + ( arg % 100 );

    // Estimating b (upper bound)
    unsigned long long bEst = arg / (20 * root);

    // Finding b rounded down
    while ( 20 * root * bEst + bEst * bEst > arg )
        --bEst;

    // Adding next significant digit (b) to root
    return root * 10 + bEst;
}
