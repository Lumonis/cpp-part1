//file      : dtor.cc
//function  : Degree to Radians function
//author    : Jeroen Lammers
//c++ ex    : ex 23

#include <iostream>
#include <cmath>

using namespace std;

double dtor(size_t degrees)
{
    degrees = degrees % 360;
    double radians = M_PI / 180 * degrees;
    return radians;
}
