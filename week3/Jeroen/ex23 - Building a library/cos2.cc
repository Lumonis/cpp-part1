//file      : cos2.cc
//function  : cos function
//author    : Jeroen Lammers
//c++ ex    : ex 23

#include <iostream>
#include <cmath>

using namespace std;

double cos(size_t degrees, size_t minutes, size_t seconds)
{
    // Returning output of sin after radians convertion
    return cos( dmstor(degrees, minutes, seconds) );
}
