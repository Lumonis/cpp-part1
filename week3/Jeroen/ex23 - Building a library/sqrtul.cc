//file      : sqrt2.cc
//function  : sqrt function
//author    : Jeroen Lammers
//c++ ex    : ex 23

#include <iostream>

using namespace std;

size_t sqrt(size_t arg)
{
    return sqrt( stoull(arg) );
}
