//file      : sin2.cc
//function  : sin function
//author    : Jeroen Lammers
//c++ ex    : ex 23

#include <iostream>
#include <cmath>

using namespace std;

double sin(size_t degrees, size_t minutes, size_t seconds)
{
    // Returning output of sin after radians convertion
    return sin( dmstor(degrees, minutes, seconds) );
}
