//file      : tan2.cc
//function  : tan function
//author    : Jeroen Lammers
//c++ ex    : ex 23

#include <iostream>
#include <cmath>

using namespace std;

double tan(size_t degrees, size_t minutes, size_t seconds)
{
    // Returning output of sin after radians convertion
    return tan( dmstor(degrees, minutes, seconds) );
}
