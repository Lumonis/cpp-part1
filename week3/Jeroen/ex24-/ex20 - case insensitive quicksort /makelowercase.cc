//file      : makelowercase.cc
//function  : Making a string lower case
//version   : 1.0
//author    : Jeroen Lammers
//c++ ex    : ex 20

#include <iostream>
#include <string>
#include <ctype.h>

using namespace std;

void makelowercase(string &imput[])
{
    for (size_t arrayIdx = 0, arrayIdx <= array.size(), ++arrayIdx)    // Looping over array elements
    {
        string arrayEntry = array[arrayIdx];                           // Defining string for ease of use
        
        for (size_t = stringIdx, stringIdx <= arrayEntry.size(), ++stringIdx) // Looping over string characters
        {
            if (tolower(arrayEntry[stringIdx]) != arrayEntry[stringIdx]) // If not lowercase, change to lower case
                arrayEntry[stringIdx] = tolower(arrayEntry[stringIdx]);
        }
    }
}