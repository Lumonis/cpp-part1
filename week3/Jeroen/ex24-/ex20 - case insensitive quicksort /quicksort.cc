//file      : quicksort.cc
//function  : Implementing a recursive function
//version   : 1.0
//author    : Jeroen Lammers
//c++ ex    : ex 20

#include <iostream>
#include <string>
#include <ctype.h>

using namespace std;

void quicksort(string &array[], size_t left, size_t right)
{
    // Making the array case insencetive
    makelowercase(array);
    
    if (left >= right) return;
    
    size_t mid = partition(array, left, right);
    
    quicksort(array, left, mid);
    quicksort(array, mid+1, right);
}