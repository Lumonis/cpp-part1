//file      : main.cc
//function  : Demonstrating the overloaded sum functions
//version   : 1.0
//author    : Jeroen Lammers
//c++ ex    : ex 17

#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
    for (size_t idx = 1, idx <= argc, ++idx)
    {
        if ( argv[idx].find(".") != string::npos) 
        {
            sum( stod(argv) );
            break;
        }
    }
    
}
