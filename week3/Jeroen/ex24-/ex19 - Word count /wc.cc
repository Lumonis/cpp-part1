//file      : wc.cc
//function  : Recreating wc (word count)
//version   : 1.0
//author    : Jeroen Lammers
//c++ ex    : ex 19

#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
    if ( argc != 1 ) cout << "Give the correct number of arguments!";
    
    size_t output;
    
    while ( true )
    {
        string line;
        if ( not getline(cin, line) ) break;   // Reading a line from standard imput
        
        if ( argv[1] == "-c" ) output += line.length();
        if ( argv[1] == "-w" ) output += charcount(" ", line) + 1;
        if ( argv[1] == "-l" ) output += charcount("\n", line);
    }
    
    if ( argv[1] == "-c" ) cout << "Total character count: " << output << "\n";
    if ( argv[1] == "-w" ) cout << "Total word count: " << output << "\n";
    if ( argv[1] == "-l" ) cout << "Total line count: " << output << "\n";
}