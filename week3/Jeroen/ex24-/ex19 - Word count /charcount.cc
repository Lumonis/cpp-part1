//file      : charcount.cc
//function  : Counting number of a certine char in a line
//version   : 1.0
//author    : Jeroen Lammers
//c++ ex    : ex 19

#include <iostream>
#include <string>

using namespace std;

size_t charcount(char delim, string imputline)
{
    size_t output = 0;
    while ( true )
    {
        string templine;
        if ( not getline(imputline, templine, delim) ) break;
        
        output += 1;        // if delim character is found, than add 1 to counter
    }
    return output;        // return number of times delim occurred in the imput
}