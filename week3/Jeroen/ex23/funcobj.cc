#include <iostream>
#include <string>
#include <cmath>

using namespace std;

double dmstor(size_t degrees, size_t minutes, size_t seconds = 0)
{
    double const degreeSize = M_PI / 180;
    double radians = degreeSize * ( degrees + minutes / 60 + seconds / 360 );
    return radians;
}

double dtor(size_t degrees)
{
    degrees = degrees % 360;
    double radians = M_PI / 180 * degrees;
    return radians;
}

double cos(size_t degrees)
{
    // Returning output of sin after degree to radians convertion
    return cos( dtor(degrees) );
}

double cos(size_t degrees, size_t minutes, size_t seconds)
{
    // Returning output of cos after radians convertion
    return cos( dmstor(degrees, minutes, seconds) );
}

double sin(size_t degrees)
{
    // Returning output of sin after degree to radians convertion
    return sin( dtor(degrees) );
}

double sin(size_t degrees, size_t minutes, size_t seconds)
{
    // Returning output of sin after radians convertion
    return sin( dmstor(degrees, minutes, seconds) );
}

double tan(size_t degrees)
{
    // Returning output of sin after degree to radians convertion
    return tan( dtor(degrees) );
}

double tan(size_t degrees, size_t minutes, size_t seconds)
{
    // Returning output of tan after radians convertion
    return tan( dmstor(degrees, minutes, seconds) );
}

unsigned long long lookupSqrt(unsigned long long arg)
{
    // Return the largest integer whose square does not exeed the value
    unsigned long long root = 0;
    while (root*root <= arg)
        ++root;
    return --root;
}

int sqrt(int arg)
{
    // Check if >0 then use ull sqrt
    if ( arg <= 0 )
        arg = -1 * arg;

    return (int) sqrt( (unsigned long long) arg );
}

size_t sqrt(size_t arg)
{
    return (int) sqrt( (unsigned long long) arg );
}

unsigned long long sqrt(unsigned long long arg)
{
    unsigned long long root = 0;

    if ( arg > 100 )                   // Rest digits of root
    root = sqrt(arg / 100);            // Finding previous digit

    if ( arg < 100 )                   // First digit of root
        return root = lookupSqrt(arg); // Looking up the first prime

    unsigned long long remainder;

    // Finding the remainder of the previous digit
    remainder = arg - 100 * root * root;

    // Prefixing the remainder to the next segment
    arg = remainder + (arg % 100);

    // Estimating b (upper bound): Since arg ~= (2 * 10 * root) * b (algorithm)
    unsigned long long bEst = arg / (20 * root);

    // Finding b rounded down
    while ( 20 * root * bEst + bEst * bEst > arg )
        --bEst;

    // Adding next significant digit (b) to root
    return root * 10 + bEst;
}

