//file      : main.cc
//function  : Testing the library
//author    : Jeroen Lammers
//c++ ex    : ex 23

#include "main.ih"

int main(int argc, char **argv)
{
    // Calling sqrt( int )
    int testint = 12345;
    cout << "int = " << testint << " : " << sqrt(testint) << '\n';

    // Calling sqrt( ull )
    unsigned long long testull = 1223560005;
    cout << "ull = " << testull << " : " << sqrt(testull) << '\n';

    // Calling sin( deg )
    size_t testdeg1 = 400;
    size_t testdeg2 = 40;
    cout << "deg = " << testdeg1 << " : " << sin(testdeg1) << '\n';
    cout << "deg = " << testdeg2 << " : " << sin(testdeg2) << '\n';

}
