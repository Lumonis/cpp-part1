//file      : sqrt2.cc
//function  : sqrt function
//author    : Jeroen Lammers
//c++ ex    : ex 23

#include "main.ih"

size_t sqrt(size_t arg)
{
    return static_cast<size_t>(sqrt( static_cast<unsigned long long>(arg) ));
}
