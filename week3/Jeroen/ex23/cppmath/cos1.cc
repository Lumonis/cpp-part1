//file      : cos1.cc
//function  : cos function
//author    : Jeroen Lammers
//c++ ex    : ex 23

#include "main.ih"

double cos(size_t degrees)
{
    // Returning output of sin after degree to radians convertion
    return cos(dtor(degrees));
}
