//file      : tan1.cc
//function  : tan function
//author    : Jeroen Lammers
//c++ ex    : ex 23

#include "main.ih"

double tan(size_t degrees)
{
    // Returning output of sin after degree to radians convertion
    return tan(dtor(degrees));
}
