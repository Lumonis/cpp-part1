//file      : lookupSqrt.cc
//function  : looking up the prime for a number below 100
//author    : Jeroen Lammers
//c++ ex    : ex 23

#include "main.ih"

unsigned long long lookupSqrt(unsigned long long arg)
{
    unsigned long long primeArray[11] = {};
    for (size_t idx = 0; idx <= 10; ++idx)
        primeArray[idx] = static_cast<unsigned long long>(idx * idx);

    // Return the largest integer whose square does not exeed the value
    unsigned long long root = 0;
    while (primeArray[root] <= arg)
        ++root;
    return --root;
}
