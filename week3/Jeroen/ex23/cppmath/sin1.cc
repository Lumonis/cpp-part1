//file      : sin1.cc
//function  : sin function
//author    : Jeroen Lammers
//c++ ex    : ex 23

#include "main.ih"

double sin(size_t degrees)
{
    // Returning output of sin after degree to radians convertion
    return sin(dtor(degrees));
}
