//file      : cos2.cc
//function  : cos function
//author    : Jeroen Lammers
//c++ ex    : ex 23

#include "main.ih"

double cos(size_t degrees, size_t minutes, size_t seconds)
{
    // Returning output of cos after radians convertion
    return cos( dmstor(degrees, minutes, seconds) );
}
