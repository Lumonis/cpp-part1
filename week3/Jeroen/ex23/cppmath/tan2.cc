//file      : tan2.cc
//function  : tan function
//author    : Jeroen Lammers
//c++ ex    : ex 23

#include "main.ih"

double tan(size_t degrees, size_t minutes, size_t seconds)
{
    // Returning output of tan after radians convertion
    return tan( dmstor(degrees, minutes, seconds) );
}
