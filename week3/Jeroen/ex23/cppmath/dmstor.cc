//file      : dmstor.cc
//function  : Deg-min-sec to Rad function
//author    : Jeroen Lammers
//c++ ex    : ex 23

#include "main.ih"

double dmstor(size_t degrees, size_t minutes, size_t seconds = 0)
{
    double const degreeSize = M_PI / 180;
    double radians = degreeSize * ( degrees + minutes / 60 + seconds / 360 );
    return radians;
}
