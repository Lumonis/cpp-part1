#ifndef INCLUDED_MYHEADER_H_
#define INCLUDED_MYHEADER_H_

#include <cmath>

int sqrt(int arg);
size_t sqrt(size_t arg);
unsigned long long sqrt(unsigned long long arg);

double sin(size_t degrees);
double cos(size_t degrees);
double tan(size_t degrees);

double sin(size_t degrees, size_t minutes, size_t seconds);
double cos(size_t degrees, size_t minutes, size_t seconds);
double tan(size_t degrees, size_t minutes, size_t seconds);

#endif
