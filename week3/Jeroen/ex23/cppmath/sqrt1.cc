//file      : sqrt1.cc
//function  : sqrt function
//author    : Jeroen Lammers
//c++ ex    : ex 23

#include "main.ih"

int sqrt(int arg)
{
    // Check if >0 then use ull sqrt
    if ( arg <= 0 )
        arg = -1 * arg;

    return static_cast<int>(sqrt( static_cast<unsigned long long>(arg) ));
}
