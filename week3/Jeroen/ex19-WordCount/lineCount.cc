//file      : linecount.cc
//function  : Counting number of lines
//author    : Jeroen Lammers
//c++ ex    : ex 19

#include "main.ih"

size_t lineCount()
{
    size_t lineCounter = 0;

    while (true)
    {
        string templine;
        if ( not getline(cin, templine) )
            break;
        ++lineCounter;
    }
    return lineCounter;
}
