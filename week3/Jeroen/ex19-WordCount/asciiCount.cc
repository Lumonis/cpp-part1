//file      : asciicount.cc
//function  : Counting number of 7bit-Ascii characters in a line
//author    : Jeroen Lammers
//c++ ex    : ex19

#include "main.ih"

size_t asciiCount()
{
    size_t output = 0;

    while (true)  // Looping for each line to find the length
    {
        string line;
        if ( not getline(cin, line) )
            break;

        output += line.length() + 1;
        // add number of 7bit-Ascii in line to output + 1 for \n
    }
    return output;
}
