//file      : wordcount.cc
//function  : Counting number of words from standard imput
//author    : Jeroen Lammers
//c++ ex    : ex 19

#include "main.ih"

size_t wordCount()
{
    size_t wordCounter = 0;

    // Since a words are either split by spaces or \n we can count them
    while (true)
    {
        string line;
        if ( not getline(cin, line, ' ') )
            break;
        ++wordCounter; // Incrementing counter for a space

        size_t lastPos = line.find('\n');
        // Look for a \n in line
        while ( lastPos != string::npos )
        {
            ++wordCounter;
            // If there is a \n, increment counter
            lastPos = line.find('\n', lastPos + 1);
            // Look for another \n after previous one
        }
    }
    // The counting is off by one due to getline using both space and eof
    //  as a delimiter thus we subtract one
    return --wordCounter;
}
