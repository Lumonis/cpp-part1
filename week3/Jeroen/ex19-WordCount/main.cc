//file : main.cc
//function : Recreating wc (word count)
//author : Jeroen Lammers
//c++ ex : ex 19

#include "main.ih"

int main(int argc, char *argv[])
{
    if ( argc != 2 )
        cout << "Give the correct number of arguments!";

    // Character count
    if ( string( argv[1] ) == "-c" )
        cout << asciiCount() << "\n";
    // Word count
    else if ( string( argv[1] ) == "-w" )
        cout << wordCount() << "\n";
    // Line count
    else if ( string( argv[1] ) == "-l" )
        cout << lineCount() << "\n";
    else
        cout << "Give a correct argument\n";
}
